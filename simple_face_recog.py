# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\PPREMAPA\simple_face_recog.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
import numpy as np
import cv2
import os
from PIL import Image



class Ui_MainWindow(object):
    cap = None
    count = 1
    list_name = []
    s = 0

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(974, 655)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.EnterName = QtWidgets.QLineEdit(self.centralwidget)
        self.EnterName.setGeometry(QtCore.QRect(10, 150, 181, 31))
        font = QtGui.QFont()
        font.setFamily("Lucida Sans Typewriter")
        self.EnterName.setFont(font)
        self.EnterName.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.EnterName.setText("")
        self.EnterName.setObjectName("EnterName")
        self.Button_Enter = QtWidgets.QPushButton(self.centralwidget)
        self.Button_Enter.setGeometry(QtCore.QRect(200, 150, 121, 31))
        self.Button_Enter.setObjectName("Button_Enter")
        self.Button_Train = QtWidgets.QPushButton(self.centralwidget)
        self.Button_Train.setGeometry(QtCore.QRect(470, 90, 231, 51))
        self.Button_Train.setObjectName("Button_Train")
        self.Button_Validate = QtWidgets.QPushButton(self.centralwidget)
        self.Button_Validate.setGeometry(QtCore.QRect(710, 90, 231, 51))
        self.Button_Validate.setObjectName("Button_Validate")
        self.video_frame = QtWidgets.QLabel(self.centralwidget)
        self.video_frame.setGeometry(QtCore.QRect(330, 150, 611, 461))
        self.video_frame.setFrameShape(QtWidgets.QFrame.Box)
        self.video_frame.setText("")
        self.video_frame.setObjectName("video_frame")
        self.listWidget = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget.setGeometry(QtCore.QRect(10, 190, 181, 192))
        self.listWidget.setObjectName("listWidget")
        self.listWidget_2 = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget_2.setGeometry(QtCore.QRect(10, 400, 300, 50))
        self.listWidget_2.setObjectName("Alerts")
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

        self.Button_Enter.clicked.connect(self.Enter_Pressed)
        self.Button_Enter.clicked.connect(self.createFolder)
        self.Button_Train.clicked.connect(self.start)
        self.Button_Enter.clicked.connect(self.start)
        #self.Button_Train.clicked.connect(self.deleteLater)


    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.Button_Enter.setText(_translate("MainWindow", "Enter"))
        self.Button_Train.setText(_translate("MainWindow", "Train "))
        self.Button_Validate.setText(_translate("MainWindow", "validate"))

    def Enter_Pressed(self):
        value = self.EnterName.text()
        self.list_name.append(value)
        self.EnterName.clear()
        self.createFolder('./User/'+value+'/')
        if self.list_name.count(value) > 1:
             self.listWidget_2.addItem('{}: {}'.format('Name already exists ',value))
        else:
            self.listWidget.addItem('ID-{}: {}'.format(self.count,value))
            self.count += 1




    # def Train_Pressed(self):
    #     #self.timer = QtCore.QTimer()
    #     self.cap = cv2.VideoCapture(0)
    #     ret, frame = self.cap.read()
    #     frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    #     img = QtGui.QImage(frame, frame.shape[1], frame.shape[0], QtGui.QImage.Format_RGB888)
    #     pix = QtGui.QPixmap.fromImage(img)
    #     self.video_frame.setPixmap(pix)
    #     self.cap.release()

    def Train_Pressed(self):
        #self.timer = QtCore.QTimer()
        value = self.EnterName.text()
        rec = cv2.face.LBPHFaceRecognizer_create()
        face_cascade = cv2.CascadeClassifier('/home/aicore/Desktop/Improvisation/haarcascade_frontalface_default.xml')
        self.cap = cv2.VideoCapture(0)
        while True:
            ret, frame = self.cap.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)
            for(x,y,w,h) in faces:
                self.s+=1
                cv2.imwrite('/home/aicore/Downloads/simple face recog/User/'+value+'/'+str(value)+"."+str(self.s)+".jpg",gray[y:y+h,x:x+w])
                cv2.rectangle(frame,(x,y),(x+w,y+h), (255,0,0), 2)
                cv2.waitKey(100)
            if (self.s>9):
                break

            # cv2.imshow('img',frame)
            # cv2.waitKey(1)
        img = QtGui.QImage(frame, frame.shape[1], frame.shape[0], QtGui.QImage.Format_RGB888)
        pix = QtGui.QPixmap.fromImage(img)
        self.video_frame.setPixmap(pix)

        self.cap.release()
        cv2.destroyAllWindows




############################################## rough #############################################
    def Validate_Pressed(self):

        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        self.cap = cv2.VideoCapture(0)
        rec = cv2.face.LBPHFaceRecognizer_create()
        rec.read("recognizer\\trainingData.xml")
        fontface = cv2.FONT_HERSHEY_SIMPLEX
        fontscale = 1
        fontcolor = (255, 255, 255)
        while True:
            ret, frame = self.cap.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            faces = face_cascade.detectMultiScale(gray, 1.3, 5)
            for(x,y,w,h)in faces:
                cv2.rectangle(frame,(x,y),(x+w,y+h), (0,0,255), 2)
                id,conf=rec.predict(gray[y:y+h,x:x+w])
                for i in range(0,len(X)):
                    print("X=:",X[i])

                    n,id_num=(X[i]).split('.')
                    print("n:",n)
                    print("id:",id)
                    print("id_num:",id_num)

                    if (str(id) == id_num):
                        cv2.putText(frame,str(n),(x,y+h),fontface, fontscale, fontcolor)

            cv2.imshow('img',frame)
            if cv2.waitKey(30) & 0xFF == ord('q'):

                break;
        cap.release()
        cv2.destroyAllWindows

        ret, frame = self.cap.read()
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        img = QtGui.QImage(frame, frame.shape[1], frame.shape[0], QtGui.QImage.Format_RGB888)
        pix = QtGui.QPixmap.fromImage(img)
        self.video_frame.setPixmap(pix)
        self.cap.release()
#############################################################################################



    def start(self):
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.Train_Pressed)
        self.timer.start(20)

    def file_save(self):
        home = '/home/aicore/Downloads/simple face recog/user.txt'
        file = open(home,'a')
        text = self.EnterName.setText("")
        file.write('done')
        file.close()

    def createFolder(self, directory):
        try:
            if not os.path.exists(directory):
                os.makedirs(directory)
        except OSError:
            print('Error: Creating directory.' + directory)






if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
